# Archlabs-qtile

Dotfiles for archlinux & qtile wm

## Preview

## Clean
![clean](/preview/clean.png)
<br />
## Dirty
![Dirty](/preview/Dirty.png)


## Details

* Distro: [ArchLabs](https://archlabslinux.com/)
* Window manager: [qtile](http://www.qtile.org/)
* Panel: [Stock Qtile + lemonbar]
* Launcher: [Rofi](https://github.com/davatorium/rofi)
* Compositor: [picom](https://github.com/jonaburg/picom)
* Terminal: [kitty & alacritty]
* Display: [1600x900]
* Music Player: [ncmpcpp]
* Text Editor: [nvim]
* File Manager: [Ranger, with w3m image previewer]
* Alternative File Manager: [thunar]
* Screen Recorder: [simplescreenrecorder]
* Program Launcher: [rofi]
* Info Fetcher: [Neofetch]
* CLI Shell Intepreter: [Zsh]
* Notification Daemon: [Dunst]
* Music Daemon: [Mpd]
* Mpd Controller: [Mpc]

## Fonts

* `NovaMono for Powerline`
* `Trebuchet-MS-Italic`
* `feather`


