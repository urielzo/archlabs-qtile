# Setup fzf
# ---------
if [[ ! "$PATH" == */home/uriel/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/uriel/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/uriel/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/uriel/.fzf/shell/key-bindings.bash"
